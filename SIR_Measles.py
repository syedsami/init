
"""
Created on %(date)s
Problem 1. Getting familiar with infection models
In order to get familiar with infection models, start by studying the SIR model.
1. Implement the SIR model in your modeling language of choice.
2. Use model parameters for the measles outbreak in Northern England, Section 2.8, and simulate the SIR model.
3. Compare the results of your simulation with the experimental data provided in Fig. 5/Table 2.



dsdt = -aIS
didt = (aS-b)I
drdt = bI
"""

import numpy as np
import matplotlib.pyplot as plt

N=763
ki = N*0.0025 #per day
kr = 0.3 # per day
Ts = 0.0002
t_start = 3
t_stop = 14
steps = int((t_stop - t_start)/Ts)
t=np.linspace(t_start,t_stop,steps)
I = np.zeros(steps)
S = np.zeros(steps)
R = np.zeros(steps)

I_original = (25,75,227,296,258,236,192,126,71,28,11,7)
I[0] = 25
S[0] = N-I[0]
R[0] = 0

for k in range(steps-1):
    S[k+1] = S[k]*(1 - ki*Ts*I[k]/N)
    I[k+1] = I[k]*(1 + (ki*S[k]/N - kr)*Ts)
    R[k+1] = R[k] + Ts*kr*I[k]
    
    


plt.plot(t,S,'b',t,I,'r',t,R,'g')
plt.scatter(range(t_start,t_stop+1,1),I_original,color='y')
plt.legend(labels=('Susceptible','Infected','Recovered','Real Infected'))
plt.xlabel('Day')
plt.ylabel('No. of pupils')
plt.title('Measles SIR model vs. Registered infection data \nof a boarding school in Northern England \nduring Jan - Feb 1978')