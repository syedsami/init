#Commit test    
import numpy as np
import matplotlib.pyplot as plt


N=60500000          #Italy
ki = .25168         #per day
ke = 1              # per day #alpha
kca = 1/7           #per day #v
nu = 0.4            #f
kc = kca*nu         #v1
ka = kca*(1-nu)     #v2
kr = 1/7            #nu
Ti = 10.5           #1/mu
c1 = 0.135          #x2
c2 = 1              #x3
x1 = 63.7           #Italy
R0 = 2.57

#Time variables
Ts = 0.0001
t_start = 0
t_stop = 200
steps = int((t_stop - t_start)/Ts)
t=np.linspace(t_start,t_stop,steps)

#Array declaration and initialization
I = np.zeros(steps)
S = np.zeros(steps)
R = np.zeros(steps)
E = np.zeros(steps)
C = np.zeros(steps)
A = np.zeros(steps)


#Models to calculate on
I[0] = (c1*c2)/kc/N
E[0] = ((c1+kca)/ke)/N
A[0] = (ka/(c1+kr))*I[0]/N
C[0] = 1/N
S[0] = N/N
R[0] = 0/N


for k in range(steps-1):
    S[k+1] = S[k] - Ts * ki * S[k] * (I[k]+A[k])
    E[k+1] = E[k] + Ts * (ki * S[k] * (I[k]+A[k]) - ke*E[k])
    I[k+1] = I[k] + Ts * (ke * E[k] - kca * I[k])
    C[k+1] = C[k] + (kc * I[k] - kr * C[k]) * Ts 
    A[k+1] = A[k] + (ka * I[k] - kr * A[k]) * Ts
    R[k+1] = R[k] + Ts*(kr * C[k] + kr * A[k])



#Ploting the graphs
plt.plot(t,S,'b',t,E,t,I,'r',t,C,t,A,t,R,'g')
#plt.scatter(range(t_start,t_stop+1,1),I_original,color='y')
plt.legend(labels=('Susceptible (S)','Incubation (E)','Infected (I)','Confirmed Infected (C)','Non-confirmed Infected (A)','Recovered (R)'))
plt.xlabel('Day')
plt.ylabel('% of population N')
plt.title('Problem 2. COVID - 19 Model for Italy')

